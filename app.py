from fastapi import FastAPI
import cv2
from pydantic import BaseModel
from typing import List
import uvicorn

import base64
import numpy as np
from ObjectDetection import OpencvYOLO
from configparser import ConfigParser


#Read config.ini file
config_object = ConfigParser()
config_object.read("config.ini")
# VEHICLE_CONFIG = config_object["VEHICLE_CONFIG"]
LICENSE_PLATE_CONFIG = config_object["LICENSE_PLATE_CONFIG"]


motorcycle_plate_cfg = LICENSE_PLATE_CONFIG["lp_cfg"]
motorcycle_plate_weight = LICENSE_PLATE_CONFIG["lp_weights"]
motorcycle_plate_name = LICENSE_PLATE_CONFIG["lp_names"].encode('utf8')


plate_detector = OpencvYOLO(cfg=motorcycle_plate_cfg, weights=motorcycle_plate_weight, objnames=motorcycle_plate_name)

#Data validation
class Location(BaseModel):
    x: int
    y: int 
    w: int 
    h: int


class Plate(BaseModel):
    label: str
    score: float
    location: Location


class Plates(BaseModel):
    __root__: List[Plate]


class RecognizeImage(BaseModel):
    image: str


app = FastAPI(title="Motorccycle plate recognition API")


@app.get('/')
async def hello_world():
    return {"Hello": "world!"}


@app.post("/detectionMotorcyclePlate/", response_model=Plates,
          summary="Motorcycle Plate Detection API",
          description="Hello world!")
async def motorcyclePlate_detection(base64_img: RecognizeImage):
    raw_image = base64_img.image
    trim_base64 = raw_image[raw_image.find(",") + 1:]
    if isBase64(trim_base64):
        processed_img = await preprocessing(base64_img.image)
        plate_list = plate_detector.getObject(processed_img, labelWant="plate",
                                                  drawBox=False)
        if len(plate_list) >= 1:
            print(len(plate_list))
            # plate_list = vehicle_detector.getResult()
            print(type(plate_list))
            return plate_list
        else:
            plate_list = [
                {
                    "label": "Object not found!",
                    "score": 0,
                    "location": {
                    "x": 0,
                    "y": 0,
                    "w": 0,
                    "h": 0
                    }
                }
            ]
            return plate_list
    else:
        return {"msg": "Invalid input image"}


# @app.post("/package/{priority}")
# async def make_package(priority: int, package: Package, value: bool):
#     return {"priority": priority, **package.dict(), "value": value}


async def preprocessing(base64_img):
    trim_base64 = base64_img[base64_img.find(",") + 1:]
    if isBase64(trim_base64):
        print("Converted to numpy ndarray")
        img_bytes = base64.b64decode(trim_base64)
        # print(img_bytes)
        img_array = np.frombuffer(img_bytes, dtype=np.uint8)  # img_array is one-dim Numpy array
        np_array = cv2.imdecode(img_array, flags=cv2.IMREAD_COLOR)

        try:
            rgb_img = cv2.cvtColor(np_array, cv2.COLOR_BGR2RGB)
        except ValueError:
            print("Could not convert empty array to RGB.")
        return rgb_img
    else:
        print("Can't convert to ndarray ")
        return False


def isBase64(sb):
    try:
        if isinstance(sb, str):
            # If there's any unicode here, an exception will be thrown and the function will return false
            sb_bytes = bytes(sb, 'ascii')
        elif isinstance(sb, bytes):
            sb_bytes = sb
        else:
            raise ValueError("Argument must be string or bytes")
        return base64.b64encode(base64.b64decode(sb_bytes)) == sb_bytes
    except Exception:
        return False


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=7200)